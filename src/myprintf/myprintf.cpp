//
// Poor man's realization of printf -- that pushes bytes to SerialUSB
//
// 2019-0403 Bob Iannucci
//

#include <stdarg.h>
#include <USB/USBAPI.h>
#include "myprintf.h"

#define PRINTF_BUF 200

// Calls to this function must come after SerialUSB has been initialized
#ifdef __cplusplus
extern "C"
#endif
void myprintf(const char *format, ...)
{
   char buf[PRINTF_BUF];
   va_list ap;
        va_start(ap, format);
        vsnprintf(buf, sizeof(buf), format, ap);
        for(char *p = &buf[0]; *p; p++) // emulate cooked mode for newlines
        {
                if(*p == '\n')
                        SerialUSB.write('\r');
                SerialUSB.write(*p);
        }
        va_end(ap);
}
